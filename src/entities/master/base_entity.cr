class JSONEntity::Master::BaseEntity
  def instance_vars
    {% for variable in @type.instance_vars %}
			 yield "{{variable}}", {{variable}}
		{% end %}
  end
end
