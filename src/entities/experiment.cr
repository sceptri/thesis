class JSONEntity::Experiment < JSONEntity::Master::BaseEntity
  include JSON::Serializable

  alias StringKeyedFloats = Hash(String, Array(Float64))

  property name : String
  property variables : StringKeyedFloats
  property parameters : StringKeyedFloats
  property equations : Hash(String, String)
  property time : StringKeyedFloats
  property languages : Array(String)
  property figures : Array(String)

  def variable_list
    keys(@variables)
  end

  def variable_equation(variable : String) : String
    if variable_list.includes(variable)
      return @equations[variable]
    end

    ""
  end
end
