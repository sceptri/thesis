class JSONEntity::Document < JSONEntity::Master::BaseEntity
  include JSON::Serializable

  property name : String

  property something : String
end
