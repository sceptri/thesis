class Components::Config < Components::Master::Windowed
  include Components::Master::Actionable
  include Components::Master::WithEntity

  takes JSONEntity::Config

  @help_message : String = "configure Thesis app"
  @title = "Config"
  @action_key = 'c'
  @default_size = 0.9

  def before_draw_entity
    @entity = Service::Config.load_first
  end

  def act_on_event(keypress : Keypress) : Bool
    toggle_on_action keypress

    super
  end
end
