class Components::Document < Components::Master::Windowed
  include Components::Master::Actionable
  include Components::Master::WithEntity

  takes JSONEntity::Document

  @help_message : String = "get to the Document section"
  @title = "Document"
  @action_key = 'd'
  @default_size = 0.5

  def before_draw_entity
    @entity = Service::Document.load_first
  end

  def act_on_event(keypress : Keypress) : Bool
    toggle_on_action keypress

    super
  end
end
