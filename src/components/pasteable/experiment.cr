class Components::Pasteable::Experiment < Components::Master::Windowed
  include Components::Master::Actionable
  include Components::Master::WithEntity

  takes JSONEntity::Experiment

  @default_size = 0.5

  def before_draw_window
    @title = @entity.try &.name || ""
    super
  end

  def act_on_event(keypress : Keypress) : Bool
    toggle_on_action keypress

    super
  end

  def clear
    deaf

    super
  end
end
