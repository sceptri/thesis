class Components::Help < Components::Master::Windowed
  include Components::Master::ForControlPanel
  include Components::Master::Actionable

  @help_message : String = "display this help dialog"
  @title = "Help"
  @panel_message = "show help"
  @action_key = 'h'

  def draw_content
    return unless (window = @window)
    window.not_nil!

    index = 1
    Thesis.help_for.each do |_, component|
      message = component.help_message
      if component.is_a?(Components::Master::Actionable)
        message = component.get_action_cue(message)
      end

      window.print message, index, 1
      index += 1
    end
  end

  def act_on_event(keypress : Keypress) : Bool
    toggle_on_action keypress

    super
  end
end
