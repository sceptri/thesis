module Components::Master::ForControlPanel
  macro included
		def self.add_to_panel
			self.instance.add_to_panel
  	end
	end

  property panel_message = ""

  def get_passive_cue(rest_of_message : String = "") : String
    rest_of_message
  end

  def add_to_panel
    Components::ControlPanel.cues << get_passive_cue @panel_message
  end
end
