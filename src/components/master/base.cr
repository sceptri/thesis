class Components::Master::Base
  def self.instance : self
    (@@instance ||= new).as(self)
  end

  property listener : Int32?

  def init
    NC.end
    raise NotImplementedError.new "Init method not implemented"
  end

  def act_on_event(keypress : Keypress) : Bool
    false
  end

  def deaf
    listener = @listener
    unless listener.nil?
      if Thesis.listeners.keys.includes?(listener)
        Thesis.listeners.delete(listener)
      end
    end

    @listener = nil
  end

  def self.deaf
    self.instance.deaf
  end

  def listen
    Thesis.add_listener(self)
  end

  def self.listen
    self.instance.listen
  end
end
