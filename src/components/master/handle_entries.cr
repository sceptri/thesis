module Components::Master::HandleEntries
  property entries = [] of Components::Master::Entry

  property current_entry : Int32? = nil

  property up_action_key : Keypress = NC::Key::Up
  property down_action_key : Keypress = NC::Key::Down

  def create_entry(window : NC::Window, x : Int32, y : Int32, name : String, value : String)
    dimensions = @dimensions
    if dimensions.nil?
      window_y = window_x = 0
    else
      _, _, window_y, window_x = dimensions
    end

    entry = Components::Master::Entry.new (window_x + x), (window_y + y), name, value
    entry.init

    if @current_entry.nil?
      @current_entry = 0
      entry.focus
    end

    entries << entry
  end

  def change_focus(last_entry_at : Int32, current_entry_at : Int32)
    if (last_entry = @entries[last_entry_at]) && (current_entry = @entries[current_entry_at])
      last_entry.lose_focus
      current_entry.focus
    end
  end

  def move_current_entry_down
    if (current_entry = @current_entry)
      last_entry = current_entry
      current_entry += 1
      if current_entry == @entries.size
        current_entry = 0
      end

      @current_entry = current_entry

      change_focus(last_entry, current_entry)
    end

    select_current_entry
  end

  def move_current_entry_up
    if (current_entry = @current_entry)
      last_entry = current_entry
      current_entry -= 1
      if current_entry == -1
        current_entry = @entries.size - 1
      end

      @current_entry = current_entry

      change_focus(last_entry, current_entry)
    end

    select_current_entry
  end

  def select_current_entry
    if @entries.size > 0 && @current_entry.nil?
      @current_entry = 0
    end
  end

  def clear_entries
    @entries.each do |entry|
      entry.deaf
    end

    @entries = [] of Components::Master::Entry
  end

  def clear
    clear_entries

    super
  end

  def act_on_event(keypress : Keypress) : Bool
    if keypress == @up_action_key
      move_current_entry_up
    elsif keypress == @down_action_key
      move_current_entry_down
    end

    super
  end
end
