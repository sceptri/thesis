class Components::Master::Entry < Components::Master::Windowed
  property x : Int32
  property y : Int32

  property name : String = ""
  property value : String = ""
  property length : Int32?

  property editing : Bool = false
  property in_focus : Bool = false

  property start_key : Keypress = Helper::Key::ENTER
  property end_key : Keypress = Helper::Key::ENTER

  @borderless = true
  @titleless = true

  def initialize(@x, @y, @name, @value)
  end

  def focus
    @in_focus = true
    listen

    redraw
  end

  def lose_focus
    @in_focus = false
    deaf

    redraw
  end

  def get_message
    "#{@name}: #{@value}"
  end

  def get_window
    @length = get_message.size
    @dimensions = {1, @length.as(Int32), @y, @x}

    NC::Window.new(*@dimensions.as(WindowDimensions))
  end

  def draw_content
    return unless (window = @window)
    window.not_nil!

    if @in_focus
      window.with_attribute NC::Attribute::Reverse do
        window.print get_message, 0, 0
      end
    else
      window.print get_message, 0, 0
    end
  end

  def resize_window
    return if get_message.size <= (@length || 0)

    clear
    init
  end

  def redraw
    resize_window

    clean_refresh

    draw_content

    refresh
  end

  def modify_value_by(keypress : Keypress)
    if keypress.is_a?(Char)
      return unless keypress.alphanumeric? || keypress == ' '

      @value = String.build do |str|
        str << @value
        str << keypress.to_s
      end
    elsif keypress == NC::Key::Backspace
      @value = @value.rchop
    end
  end

  def act_on_event(keypress : Keypress) : Bool
    if keypress == @end_key && @editing
      @editing = false

      Thesis.only_listen = nil
      Components::ControlPanel.state = nil
    elsif keypress == @start_key && !@editing
      @editing = true

      Thesis.only_listen = @listener
      Components::ControlPanel.state = "Editing"
    elsif Thesis.only_listen == @listener && !@listener.nil?
      modify_value_by keypress

      redraw
    end

    super
  end
end
