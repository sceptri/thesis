class Components::Master::Windowed < Components::Master::Base
  alias WindowDimensions = Tuple(Int32, Int32, Int32, Int32)

  property window : NC::Window?
  property dimensions : WindowDimensions?

  property title : String = ""
  property help_message : String = ""
  property help_at : Int32?

  property default_size : Float32 = 0.6

  property borderless : Bool = false
  property titleless : Bool = false

  def show_in_help
    Thesis.add_to_help(self)
  end

  def self.show_in_help
    self.instance.show_in_help
  end

  def remove_from_help
    help_at_index = @help_at
    unless help_at_index.nil?
      Thesis.help_for.delete(help_at_index)
    end
  end

  def self.remove_from_help
    self.instance.remove_from_help
  end

  def listen_and_help
    self.listen
    self.show_in_help
  end

  def self.listen_and_help
    self.instance.listen_and_help
  end

  def before_draw_window
  end

  def draw_content
  end

  def after_draw_window
  end

  def init
    before_draw_window

    window = get_window
    @window = window

    draw_border
    draw_title
    draw_content

    NC.refresh
    window.refresh

    after_draw_window
  end

  def get_window : NC::Window
    default_window
  end

  def default_window(percentage : Float = @default_size)
    dimensions = Helper.window_dimensions(percentage)
    @dimensions = dimensions
    NC::Window.new(*dimensions)
  end

  def draw_border
    return unless (window = @window)
    window.not_nil!

    if @borderless
      window.no_border
    else
      window.border
    end
  end

  def draw_title
    return unless (window = @window)
    window.not_nil!

    if (title = @title) && window && !@titleless
      window.with_attribute(NC::Attribute::Bold) do
        title = " #{title} "
        window.print title, 0, (window.width / 2) - (title.size / 2)
      end
    end
  end

  def clean_refresh
    @window.try &.clear
    refresh
  end

  def refresh
    @window.try &.refresh
    NC.refresh
  end

  def clear
    window = @window
    unless window.nil?
      window.clear
      window.refresh
      window.delete_window
      NC.refresh

      @window = nil
    end
  end
end
