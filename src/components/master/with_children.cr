module Components::Master::WithChildren
  macro children_are(type)
		property children = [] of {{type}}

		def create_child(entity, action_key)
			child = {{type}}.new entity
			child.action_key = action_key
			child.listen

			children << child
		end

		def init
			@children = [] of {{type}}

			super
		end

		def clear_children
			incoming_component = Thesis.incoming_component
			use_component : {{type}}? = nil

			@children.each do |child|
				if child == incoming_component
					use_component = child
				else
					child.deaf
				end
			end

			@children = use_component.nil? ? [] of {{type}} : [use_component]
		end

		def clear
			clear_children

			super
		end
	end
end
