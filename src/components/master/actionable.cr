module Components::Master::Actionable
  property action_key : Keypress

  def toggle_on_action(keypress : Keypress, remember = true)
    if keypress == action_key
      Thesis.switch_or_return self, remember: remember
    end
  end

  def get_passive_cue(rest_of_message : String = "") : String
    "'#{@action_key}' to #{rest_of_message}"
  end

  def get_action_cue(rest_of_message : String = "") : String
    "Press #{get_passive_cue rest_of_message}"
  end
end
