module Components::Master::WithEntity
  include Components::Master::HandleEntries

  macro takes(entity_type)
		property entity : {{entity_type}}?

		def initialize(@entity : {{entity_type}}? = nil)
		end
	end

  def before_draw_entity
  end

  def draw_entity(offset : Int32 = 0)
    before_draw_entity

    window = @window
    return if window.nil?

    window.not_nil!

    entity = @entity
    index = 1
    unless entity.nil?
      entity.instance_vars do |name, value|
        create_entry(window, 1, offset + index, name, value.to_s)
        index += 1
      end
    end
  end

  def after_draw_window
    draw_entity
    super
  end
end
