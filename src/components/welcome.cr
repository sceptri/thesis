class Components::Welcome < Components::Master::Windowed
  include Components::Master::Actionable

  @help_message : String = "display Welcome screen"
  @title = "Welcome"
  @action_key = 'w'

  @titleless = true
  @default_size = 1.0

  def draw_content
    return unless (window = @window)
    window.not_nil!

    message = "Welcome to 'Thesis'"
    window.with_attribute(NC::Attribute::Bold) do
      window.print message, 0.9 * window.height / 2, (window.width / 2) - (message.size / 2)
    end

    submessage = "'Thesis' is a management app for my bachelor thesis"
    window.print submessage, 1.1 * window.height / 2, (window.width / 2) - (submessage.size / 2)
    submessage = "You are at #{Dir.current}"
    window.print submessage, 1.2 * window.height / 2, (window.width / 2) - (submessage.size / 2)
  end

  def act_on_event(keypress : Keypress) : Bool
    toggle_on_action keypress

    super
  end
end
