class Components::Entity < Components::Master::Windowed
  include Components::Master::Actionable
  include Components::Master::WithEntity

  # TODO: rework as JSON::Any
  takes JSONEntity::Experiment

  @action_key = 'a'
  @default_size = 0.9

  def draw_entity(window : NC::Window)
    window.print (@entity.try &.name || ""), 1, 1
  end

  def act_on_event(keypress : Keypress) : Bool
    toggle_on_action keypress

    super
  end
end
