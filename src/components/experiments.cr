class Components::Experiments < Components::Master::Windowed
  include Components::Master::Actionable
  include Components::Master::WithChildren

  children_are Components::Pasteable::Experiment

  @help_message : String = "get to the Experiments section"
  @title = "Experiments"
  @action_key = 'e'
  @default_size = 0.9

  def draw_content
    return unless (window = @window)
    window.not_nil!

    index = 1
    Service::Experiments.load_all.each do |experiment|
      unless experiment.nil?
        window.print "#{index}) #{experiment.name}", index, 1

        create_child(experiment, index.to_s[0])

        index += 1
      end
    end
  end

  def act_on_event(keypress : Keypress) : Bool
    toggle_on_action keypress

    super
  end
end
