class Components::ControlPanel < Components::Master::Windowed
  include Components::Master::ForControlPanel
  include Components::Master::Actionable

  property cues : Array(String) = [] of String
  property state : String?

  @action_key = 'q'
  @panel_message = "quit"

  @titleless = true
  @borderless = true

  def get_window
    @dimensions = {1, NC.width - 1, NC.height - 1, 0}
    NC::Window.new(*@dimensions.as(WindowDimensions))
  end

  def draw_content
    message = String.build do |str|
      if (state = @state)
        str << "["
        str << state
        str << "] "
      end

      str << "Press "
      str << cues.join(',')
    end
    @window.try &.print(message, 0, 0)
  end

  def act_on_event(keypress : Keypress) : Bool
    if keypress == NC::Key::Backspace
      Thesis.return
    end
    keypress == 'q'
  end

  def self.cues
    self.instance.cues
  end

  def self.state
    self.instance.state
  end

  def self.state=(state : String?)
    self.instance.state = state

    self.instance.clear
    self.instance.init
  end
end
