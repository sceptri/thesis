require "ncurses"
require "json"

alias NC = NCurses
alias Keypress = NC::Key | Char | Nil

require "./requires"

class Thesis < Controller
  def self.run
    init

    Components::ControlPanel.listen
    Components::Welcome.listen_and_help
    Components::Help.listen_and_help
    Components::Experiments.listen_and_help
    Components::Document.listen_and_help
    Components::Config.listen_and_help

    Components::Help.add_to_panel
    Components::ControlPanel.add_to_panel

    switch Components::Welcome.instance

    listen

    close
  end
end

Thesis.run
