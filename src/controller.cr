class Controller
  alias WindowedComponent = Components::Master::Windowed
  alias BaseComponent = Components::Master::Base

  @@listeners = {} of Int32 => Keypress -> Bool
  @@help_for = {} of Int32 => WindowedComponent

  @@listener_index : Int32 = 1
  @@help_index : Int32 = 1

  @@active_component : WindowedComponent?
  @@last_component : WindowedComponent?
  @@incoming_component : WindowedComponent?

  @@only_listen : Int32?

  @@default_component : WindowedComponent? = Components::Welcome.instance

  @@home_directory = Dir.current

  def self.home_directory
    @@home_directory
  end

  def self.listeners
    @@listeners
  end

  def self.help_for
    @@help_for
  end

  def self.only_listen=(to : Int32? = nil)
    @@only_listen = to
  end

  def self.only_listen
    @@only_listen
  end

  def self.switch_or_return(object : WindowedComponent, remember = true)
    if Thesis.active_component == object
      Thesis.switch remember: remember
    else
      Thesis.switch object, remember: remember
    end
  end

  def self.return
    switch
    @@last_component = @@default_component
  end

  def self.switch(object : WindowedComponent = @@last_component, remember = true)
    if object.nil? && @@last_component.nil?
      return
    end

    @@incoming_component = object

    clear_all

    if (active_component = @@active_component)
      active_component.clear
    end

    @@last_component = remember ? active_component : @@default_component
    @@active_component = object

    unless object.nil?
      object.init
    end

    @@incoming_component = nil
  end

  def self.incoming_component
    @@incoming_component
  end

  def self.last_component
    @@last_component
  end

  def self.active_component
    @@active_component
  end

  def self.active_component=(object : WindowedComponent)
    @@active_component = object
  end

  def self.init
    NC.start
    NC.cbreak
    NC.no_echo

    # NC.no_delay

    NC.keypad true
    NC.set_cursor NC::Cursor::Invisible
  end

  def self.close
    NC.end
  end

  def self.listen
    NC.get_char do |keypress|
      any_quit = false

      @@listeners.each do |index, listener|
        if @@only_listen.nil? || @@only_listen == index
          any_quit ||= listener.call(keypress)
        end
      end

      break if any_quit
    end
  end

  def self.add_listener(object : BaseComponent)
    @@listeners[@@listener_index] = ->object.act_on_event(Keypress)
    object.listener = @@listener_index

    @@listener_index += 1
  end

  def self.add_to_help(component : WindowedComponent)
    @@help_for[@@help_index] = component
    component.help_at = @@help_index

    @@help_index += 1
  end

  def self.clear_all
    NC.clear
    NC.refresh

    Components::ControlPanel.instance.init
  end

  def self.run
    NC.end
    raise NotImplementedError.new "Run method not implemented"
  end
end
