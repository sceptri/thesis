module Helper
  extend self

  def window_dimensions(percent_filled : Float = 0.6) : Tuple(Int32, Int32, Int32, Int32)
    width = NC.width
    height = NC.height - 1

    if percent_filled < 0 || percent_filled > 1
      percent_filled = 1
    end

    {
      (height * percent_filled).to_i,
      (width * percent_filled).to_i,
      (height * (1 - percent_filled) / 2).to_i,
      (width * (1 - percent_filled) / 2).to_i,
    }
  end

  module Key
    extend self

    ENTER = '\n'
  end
end
