class Service::Master::Runner
  @@directory = "."

  @@command : String = ""
  @@args = [] of String

  @@use_file_extension : Bool = true
  @@file_extension : String = ".txt"

  getter output : String = ""
  getter error : String = ""

  def add_file_to(args : Array(String), filename : String) : Array(String)
    file_extension = @@use_file_extension ? @@file_extension : ""
    args << "#{filename}#{file_extension}"
    args
  end

  def run(filename : String) : Bool
    args = @@args
    args = add_file_to(args, filename)

    process = Process.new(@@command, args, output: Process::Redirect::Pipe, error: Process::Redirect::Pipe)
    @output = process.output.gets_to_end
    @error = process.error.gets_to_end
    process.wait.success?
  rescue exception
    @error = exception.to_s
    false
  end
end
