class Service::Master::Loadable
  @@entity : JSONEntity::Master::BaseEntity.class = JSONEntity::Master::BaseEntity

  @@config : String = "config.json"
  @@folder_name : String = ""
  @@singular : Bool = false

  def self.get_folder_by(name : String)
    if @@singular
      get_folder
    else
      "#{get_folder}/#{name}"
    end
  end

  def self.get_folder
    home_directory = Thesis.home_directory
    "#{home_directory}/#{@@folder_name}"
  end

  macro def_load_one(entity_type)
		def self.load_one(name : String) : {{entity_type}}?
			service_folder = get_folder_by name
			return unless Dir.exists? service_folder
			return unless File.exists? "#{service_folder}/#{@@config}"

			config = File.read "#{service_folder}/#{@@config}"
			begin
				entity = {{entity_type}}.from_json(config)
			rescue
				entity = nil
			end
			entity
		end
	end

  macro def_load_all(entity_type)
		def self.load_all : Iterator({{entity_type}}?)
			service_folder = get_folder
			return ([] of {{entity_type}}?).each unless Dir.exists? service_folder
			Dir.children(service_folder).each.map do |folder|
				load_one folder
			end
		end
	end

  macro def_load_first(entity_type)
		def self.load_first : {{entity_type}}?
			if @@singular
				return load_one @@folder_name
			else
				return load_all.first?
			end
		end
	end

  macro takes(entity_type)
		@@entity = {{entity_type}}

		def_load_one {{entity_type}}
		def_load_all {{entity_type}}
		def_load_first {{entity_type}}
	end
end
