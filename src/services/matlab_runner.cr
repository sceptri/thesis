class Service::MatlabRunner < Service::Master::Runner
  # ----- Example usage -----
  #
  # runner = Service::MatlabRunner.new
  # command = runner.run("hello")
  # if command
  #   print runner.output
  # else
  #		print runner.error
  # end

  # Command MUST point to the exact location of the executable
  # Otherwise it will NOT work
  # TODO: move to config
  @@command = "matlab"
  @@args = ["-batch"]

  @@file_extension = ".m"
  @@use_file_extension = false
end
