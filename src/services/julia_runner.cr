class Service::JuliaRunner < Service::Master::Runner
  # ----- Example usage -----
  #
  # runner = Service::JuliaRunner.new
  # command = runner.run("hello")
  # if command
  #   print runner.output
  # else
  #		print runner.error
  # end

  @@command = "julia"
  @@file_extension = ".jl"
end
