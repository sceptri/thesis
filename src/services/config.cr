class Service::Config < Service::Master::Loadable
  @@folder_name = "."
  @@singular = true

  takes JSONEntity::Config
end
