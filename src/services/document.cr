class Service::Document < Service::Master::Loadable
  @@folder_name = "document"
  @@singular = true

  takes JSONEntity::Document
end
