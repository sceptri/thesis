class Service::Experiments < Service::Master::Loadable
  @@folder_name = "experiments"

  takes JSONEntity::Experiment
end
